import GeneralQuestionsIcon from './images/generalQuestions.svg';
import OwnershipFormIcon from './images/ownershipForm.svg';
import OwnershipFormOnSelectIcon from './images/ownershipFormOnSelect.svg';
import AddressIcon from './images/address.svg';
import SocialNetworksIcon from './images/socialNetworks.svg';

export {
  GeneralQuestionsIcon,
  OwnershipFormIcon,
  OwnershipFormOnSelectIcon,
  AddressIcon,
  SocialNetworksIcon,
};